/*
  Apache Thrift Server for Stanford CoreNLP (stanford-thrift)
  Copyright (C) 2013 Diane M. Napolitano, Educational Testing Service
  
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation, version 2
  of the License.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import org.apache.thrift.TApplicationException;
import edu.stanford.nlp.pipeline.Annotation;

import parser.StanfordParserThrift;
import CoreNLP.*;

import general.CoreNLPThriftConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;


public class StanfordCoreNLPHandler implements StanfordCoreNLP.Iface 
{
    private StanfordParserThrift parser;
    
    // TODO: This NEEDS to be able to accept paths to alternate models other than just the Parser.
/*    public StanfordCoreNLPHandler(String parserModelFilePath)
    {
    	System.err.println("Initializing Parser...");
    	parser = new StanfordParserThrift(parserModelFilePath);
    }
*/
    
    public StanfordCoreNLPHandler(String configFilePath) throws Exception
    {
    	try
    	{
        	System.err.println("Reading in configuration from " + configFilePath + "...");
    		CoreNLPThriftConfig config = new CoreNLPThriftConfig(configFilePath);
    		System.err.println("Initializing Parser...");
    		parser = new StanfordParserThrift(config.getSRParserModel(), config.getTaggerModel(), config.getSentimentModel(), config.getNERModels());
    		//System.err.println("Initializing Tregex...");
    		//tregex = new StanfordTregexThrift();
    		//System.err.println("Initializing Tagger...");
    		//tagger = new StanfordTaggerThrift(config.getTaggerModel());
    		System.err.println("Initializing Tokenizer...");
    	}
    	catch (Exception e)
    	{
    		throw e;
    	}
    }
    
    /* Begin Stanford Parser methods */
    public FullParse parse_text(String text, List<String> outputFormat) throws TApplicationException
    {
        try {
    	    if (outputFormat == null)
    	    {
    		List<String> oF = new ArrayList<String>();
    		return parser.parse_text(text, oF);
    	    }
    	    return parser.parse_text(text, outputFormat);
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new TApplicationException(TApplicationException.INTERNAL_ERROR, e.getMessage());
        }
    }

    
    public void ping() 
    {
        System.out.println("ping()");
    }

    public void zip() 
    {
        System.out.println("zip()");
    }
}
