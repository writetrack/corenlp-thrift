/*
  Apache Thrift Server for Stanford CoreNLP (stanford-thrift)
  Copyright (C) 2013 Diane M. Napolitano, Educational Testing Service
  
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation, version 2
  of the License.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package parser;

import CoreNLP.*;

import org.apache.thrift.TApplicationException;
import java.lang.IndexOutOfBoundsException;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Properties;
import java.text.DecimalFormat;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;
import java.util.Collections;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.trees.NamedDependency;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.EnglishGrammaticalStructureFactory;
import edu.stanford.nlp.trees.UniversalSemanticHeadFinder;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.parser.shiftreduce.ShiftReduceParser;
import edu.stanford.nlp.pipeline.SentimentAnnotator;
import edu.stanford.nlp.util.ReflectionLoading;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.NERCombinerAnnotator;
import edu.stanford.nlp.pipeline.BinarizerAnnotator;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.Filters;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefChain.CorefMention;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.pipeline.NERCombinerAnnotator;

import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import org.ejml.simple.SimpleMatrix;
import java.io.IOException;
import java.lang.ClassNotFoundException;

import org.languagetool.JLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;

public class StanfordParserThrift
{
    private TokenizerFactory<? extends CoreLabel> tokenizerFactory;
    private ShiftReduceParser parser;
    private MaxentTagger tagger;
    private SentimentAnnotator sentiment;
    private StanfordCoreNLP coref;
    private final GrammaticalStructureFactory gsf;
    private static final NumberFormat NF = new DecimalFormat("0.0000");  
    private NERCombinerAnnotator ner;
    private BinarizerAnnotator binarizer;
    private int tok_index;
    private AmericanEnglish english;


    public StanfordParserThrift(String modelFile, String taggerModel, String sentimentModel, List<String> nerModels) throws IOException, ClassNotFoundException {
        String[] models = new String[nerModels.size()];
        nerModels.toArray(models);
        ner = new NERCombinerAnnotator(false, models);

        tok_index=0;

        english = new AmericanEnglish();

        Properties props = new Properties();
        props.put("annotators", "sentiment");
        sentiment = new SentimentAnnotator("", props);

	tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "invertible,ptb3Escaping=true");
        tagger = new MaxentTagger(taggerModel);
        loadModel(modelFile);
        gsf = new EnglishGrammaticalStructureFactory(Filters.acceptFilter());

        Properties props2 = new Properties();
        props2.put("annotators", "dcoref");
        props2.put("dcoref.allowReparsing", "false");
        coref = new StanfordCoreNLP(props2, false);

        Properties props3 = new Properties();
        props3.put("annotators", "binarizer");
        binarizer = new BinarizerAnnotator("", props3);
    }

    private void loadModel(String modelFile)
    {
        parser = ShiftReduceParser.loadModel(modelFile, new String[]{});
    }

    public FullParse parse_text(String text, List<String> outputOptions) throws TApplicationException, IOException {
        List<Suggestion> suggestions = new ArrayList<Suggestion>();
        if (outputOptions.contains("languagetool")) {
            JLanguageTool langTool = new JLanguageTool(english);
            List<RuleMatch> matches = langTool.check(text);
            for (RuleMatch match : matches) {
                Rule rule = match.getRule();
                suggestions.add(new Suggestion(
                    match.getFromPos(),
                    match.getToPos(),
                    match.getMessage(),
                    match.getSuggestedReplacements(),
                    rule.getId(),
                    rule.getCategory().getId().toString()
                ));
            }
        }
        Morphology morph = new Morphology();
        List<Sentence> sentence_list = new ArrayList<Sentence>();
        List<List<Span>> coref_list = new ArrayList<List<Span>>();
        FullParse results = new FullParse(sentence_list, suggestions);
        try {
            DocumentPreprocessor preprocess = new DocumentPreprocessor(new StringReader(text));
            preprocess.setTokenizerFactory(tokenizerFactory);
            Iterator<List<HasWord>> sentences = preprocess.iterator();
            List<CoreMap> annotatedSentences = new ArrayList<CoreMap>();

            int tokenIndex = 0;
            while (sentences.hasNext()) {
                List<CoreLabel> core_sentence = ((List<CoreLabel>)(List<?>)sentences.next());
                tagger.tagCoreLabels(core_sentence);
                Tree parseTree = parser.apply(core_sentence);
                GrammaticalStructure gs = gsf.newGrammaticalStructure(parseTree);
                List<Token> token_list = new ArrayList<Token>();
                for (CoreLabel label : core_sentence) {
                      morph.stem(label);
                      Token tmp = new Token(label.originalText().toLowerCase(), label.tag(), label.lemma().toLowerCase(), label.beginPosition(), label.endPosition(), tokenIndex);
                      token_list.add(tmp);
                    tokenIndex = tokenIndex + 1;
                }
                Map<String, List<Dependency>> dep_map = new HashMap<String, List<Dependency>>();
                for (TypedDependency dep : gs.typedDependencies()) {
                    if (!dep_map.containsKey(dep.reln().toString())) {
                        dep_map.put(dep.reln().toString(), new ArrayList<Dependency>());
                    }
                    dep_map.get(dep.reln().toString()).add(new Dependency(dep.gov().index() - 1, dep.dep().index() - 1));
                }
                String sentence_text = text.substring(token_list.get(0).start_index, token_list.get(token_list.size() - 1).end_index);
                Sentence sentence = new Sentence(parseTree.toString(), token_list, dep_map, sentence_list.size());
                CoreMap sentence_map = new Annotation(sentence_text);
                sentence_map.set(CoreAnnotations.TokensAnnotation.class, core_sentence);
                sentence_map.set(TreeCoreAnnotations.TreeAnnotation.class, parseTree);
                List<CoreMap> sentences_map = Collections.singletonList(sentence_map);
                Annotation annotation = new Annotation("");
                annotation.set(CoreAnnotations.SentencesAnnotation.class, sentences_map);
                if (outputOptions.contains("sentiment")) {
                    Map<String, String> sent_map = new HashMap<String, String>();
                    binarizer.annotate(annotation);
                    sentiment.annotate(annotation);
                    sentence.setSentiment(sentence_map.get(SentimentCoreAnnotations.SentimentClass.class));
                    Tree tree = sentence_map.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
                    setIndexLabels(tree, 0);
                    sent_map.put("sentiment_tree", tree.toString());
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    PrintStream ps = new PrintStream(baos, true, "utf-8");
                    outputTreeScores(token_list, ps, tree, 0);
                    sent_map.put("sentiment_matrix", baos.toString());
                    sentence.setAttributes(sent_map);
                    tok_index = 0;
                }
                if (outputOptions.contains("coref")) {
                    ner.annotate(annotation);
                    annotatedSentences.add(sentence_map);
                }
                sentence_list.add(sentence);
            }
            results.setSentences(sentence_list);
            if (outputOptions.contains("coref")) {
                Annotation coref_annotation = new Annotation(annotatedSentences);
                coref.annotate(coref_annotation);
                Map<Integer, CorefChain> corefChains = coref_annotation.get(CorefCoreAnnotations.CorefChainAnnotation.class);
                if (corefChains != null) {
                    for (CorefChain chain : corefChains.values()) {
                        List<Span> span_list = new ArrayList<Span>();
                        for (CorefMention m : chain.getMentionsInTextualOrder()) {
                            span_list.add(new Span(m.headIndex - 1, m.endIndex - 1, m.sentNum - 1));
                        }
                        coref_list.add(span_list);
                    }
                }
                results.setCorefs(coref_list);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new TApplicationException(TApplicationException.INTERNAL_ERROR, e.getMessage());
        }


        return results;
    }


    private int outputTreeScores(List<Token> tok_list, PrintStream out, Tree tree, int index) {
      if (tree.isLeaf()) {
          return index;
      }

      out.print("  " + index + ":");
      SimpleMatrix vector = RNNCoreAnnotations.getPredictions(tree);
      Map<String, String> tok_attr = new HashMap<String, String>();
      for (int i = 0; i < vector.getNumElements(); ++i) {
          out.print("  " + NF.format(vector.get(i)));
          if (tree.numChildren() == 1) {
              tok_attr.put("sentiment" + i, NF.format(vector.get(i)));
          }
      }
      if (tree.numChildren() == 1) {
          tok_list.get(tok_index).setAttributes(tok_attr);
          tok_index++;
      }
      out.println();
      index++;
      for (Tree child : tree.children()) {
          index = outputTreeScores(tok_list, out, child, index);
      }
      return index;
    }

    static int setIndexLabels(Tree tree, int index) {
      if (tree.isLeaf()) {
          return index;
      }

      tree.label().setValue(Integer.toString(index));
      index++;
      for (Tree child : tree.children()) {
          index = setIndexLabels(child, index);
      }
      return index;
    }

    static void setSentimentLabels(Tree tree) {
      if (tree.isLeaf()) {
          return;
      }

      for (Tree child : tree.children()) {
          setSentimentLabels(child);
      }

      Label label = tree.label();
      if (!(label instanceof CoreLabel)) {
          throw new IllegalArgumentException("Required a tree with CoreLabels");
      }
      CoreLabel cl = (CoreLabel) label;
      cl.setValue(Integer.toString(RNNCoreAnnotations.getPredictedClass(tree)));
    }

}
