#
# Autogenerated by Thrift Compiler (0.9.3)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py:utf8strings,slots,new_style
#

from thrift.Thrift import TType, TMessageType, TException, TApplicationException

from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol, TProtocol
try:
  from thrift.protocol import fastbinary
except:
  fastbinary = None



class Token(object):
  """
  Attributes:
   - word
   - pos
   - lemma
   - start_index
   - end_index
   - index
   - nextt
   - previous
   - attributes
  """

  __slots__ = [ 
    'word',
    'pos',
    'lemma',
    'start_index',
    'end_index',
    'index',
    'nextt',
    'previous',
    'attributes',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.STRING, 'word', None, None, ), # 1
    (2, TType.STRING, 'pos', None, None, ), # 2
    (3, TType.STRING, 'lemma', None, None, ), # 3
    (4, TType.I32, 'start_index', None, None, ), # 4
    (5, TType.I32, 'end_index', None, None, ), # 5
    (6, TType.I32, 'index', None, None, ), # 6
    (7, TType.I32, 'nextt', None, None, ), # 7
    (8, TType.I32, 'previous', None, None, ), # 8
    (9, TType.MAP, 'attributes', (TType.STRING,None,TType.STRING,None), {
    }, ), # 9
  )

  def __init__(self, word=None, pos=None, lemma=None, start_index=None, end_index=None, index=None, nextt=None, previous=None, attributes=thrift_spec[9][4],):
    self.word = word
    self.pos = pos
    self.lemma = lemma
    self.start_index = start_index
    self.end_index = end_index
    self.index = index
    self.nextt = nextt
    self.previous = previous
    if attributes is self.thrift_spec[9][4]:
      attributes = {
    }
    self.attributes = attributes

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.STRING:
          self.word = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.STRING:
          self.pos = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.STRING:
          self.lemma = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.I32:
          self.start_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 5:
        if ftype == TType.I32:
          self.end_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 6:
        if ftype == TType.I32:
          self.index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 7:
        if ftype == TType.I32:
          self.nextt = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 8:
        if ftype == TType.I32:
          self.previous = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 9:
        if ftype == TType.MAP:
          self.attributes = {}
          (_ktype1, _vtype2, _size0 ) = iprot.readMapBegin()
          for _i4 in xrange(_size0):
            _key5 = iprot.readString().decode('utf-8')
            _val6 = iprot.readString().decode('utf-8')
            self.attributes[_key5] = _val6
          iprot.readMapEnd()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Token')
    if self.word is not None:
      oprot.writeFieldBegin('word', TType.STRING, 1)
      oprot.writeString(self.word.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.pos is not None:
      oprot.writeFieldBegin('pos', TType.STRING, 2)
      oprot.writeString(self.pos.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.lemma is not None:
      oprot.writeFieldBegin('lemma', TType.STRING, 3)
      oprot.writeString(self.lemma.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.start_index is not None:
      oprot.writeFieldBegin('start_index', TType.I32, 4)
      oprot.writeI32(self.start_index)
      oprot.writeFieldEnd()
    if self.end_index is not None:
      oprot.writeFieldBegin('end_index', TType.I32, 5)
      oprot.writeI32(self.end_index)
      oprot.writeFieldEnd()
    if self.index is not None:
      oprot.writeFieldBegin('index', TType.I32, 6)
      oprot.writeI32(self.index)
      oprot.writeFieldEnd()
    if self.nextt is not None:
      oprot.writeFieldBegin('nextt', TType.I32, 7)
      oprot.writeI32(self.nextt)
      oprot.writeFieldEnd()
    if self.previous is not None:
      oprot.writeFieldBegin('previous', TType.I32, 8)
      oprot.writeI32(self.previous)
      oprot.writeFieldEnd()
    if self.attributes is not None:
      oprot.writeFieldBegin('attributes', TType.MAP, 9)
      oprot.writeMapBegin(TType.STRING, TType.STRING, len(self.attributes))
      for kiter7,viter8 in self.attributes.items():
        oprot.writeString(kiter7.encode('utf-8'))
        oprot.writeString(viter8.encode('utf-8'))
      oprot.writeMapEnd()
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.word)
    value = (value * 31) ^ hash(self.pos)
    value = (value * 31) ^ hash(self.lemma)
    value = (value * 31) ^ hash(self.start_index)
    value = (value * 31) ^ hash(self.end_index)
    value = (value * 31) ^ hash(self.index)
    value = (value * 31) ^ hash(self.nextt)
    value = (value * 31) ^ hash(self.previous)
    value = (value * 31) ^ hash(self.attributes)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class Dependency(object):
  """
  Attributes:
   - governor_index
   - dependent_index
  """

  __slots__ = [ 
    'governor_index',
    'dependent_index',
   ]

  thrift_spec = (
    None, # 0
    None, # 1
    (2, TType.I32, 'governor_index', None, None, ), # 2
    None, # 3
    (4, TType.I32, 'dependent_index', None, None, ), # 4
  )

  def __init__(self, governor_index=None, dependent_index=None,):
    self.governor_index = governor_index
    self.dependent_index = dependent_index

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 2:
        if ftype == TType.I32:
          self.governor_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.I32:
          self.dependent_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Dependency')
    if self.governor_index is not None:
      oprot.writeFieldBegin('governor_index', TType.I32, 2)
      oprot.writeI32(self.governor_index)
      oprot.writeFieldEnd()
    if self.dependent_index is not None:
      oprot.writeFieldBegin('dependent_index', TType.I32, 4)
      oprot.writeI32(self.dependent_index)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.governor_index)
    value = (value * 31) ^ hash(self.dependent_index)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class Sentence(object):
  """
  Attributes:
   - tree
   - tokens
   - dependencies
   - index
   - sentiment
   - nexts
   - previous
   - attributes
  """

  __slots__ = [ 
    'tree',
    'tokens',
    'dependencies',
    'index',
    'sentiment',
    'nexts',
    'previous',
    'attributes',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.STRING, 'tree', None, None, ), # 1
    (2, TType.LIST, 'tokens', (TType.STRUCT,(Token, Token.thrift_spec)), None, ), # 2
    (3, TType.MAP, 'dependencies', (TType.STRING,None,TType.LIST,(TType.STRUCT,(Dependency, Dependency.thrift_spec))), None, ), # 3
    (4, TType.I32, 'index', None, None, ), # 4
    (5, TType.STRING, 'sentiment', None, None, ), # 5
    (6, TType.I32, 'nexts', None, None, ), # 6
    (7, TType.I32, 'previous', None, None, ), # 7
    (8, TType.MAP, 'attributes', (TType.STRING,None,TType.STRING,None), {
    }, ), # 8
  )

  def __init__(self, tree=None, tokens=None, dependencies=None, index=None, sentiment=None, nexts=None, previous=None, attributes=thrift_spec[8][4],):
    self.tree = tree
    self.tokens = tokens
    self.dependencies = dependencies
    self.index = index
    self.sentiment = sentiment
    self.nexts = nexts
    self.previous = previous
    if attributes is self.thrift_spec[8][4]:
      attributes = {
    }
    self.attributes = attributes

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.STRING:
          self.tree = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.LIST:
          self.tokens = []
          (_etype12, _size9) = iprot.readListBegin()
          for _i13 in xrange(_size9):
            _elem14 = Token()
            _elem14.read(iprot)
            self.tokens.append(_elem14)
          iprot.readListEnd()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.MAP:
          self.dependencies = {}
          (_ktype16, _vtype17, _size15 ) = iprot.readMapBegin()
          for _i19 in xrange(_size15):
            _key20 = iprot.readString().decode('utf-8')
            _val21 = []
            (_etype25, _size22) = iprot.readListBegin()
            for _i26 in xrange(_size22):
              _elem27 = Dependency()
              _elem27.read(iprot)
              _val21.append(_elem27)
            iprot.readListEnd()
            self.dependencies[_key20] = _val21
          iprot.readMapEnd()
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.I32:
          self.index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 5:
        if ftype == TType.STRING:
          self.sentiment = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 6:
        if ftype == TType.I32:
          self.nexts = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 7:
        if ftype == TType.I32:
          self.previous = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 8:
        if ftype == TType.MAP:
          self.attributes = {}
          (_ktype29, _vtype30, _size28 ) = iprot.readMapBegin()
          for _i32 in xrange(_size28):
            _key33 = iprot.readString().decode('utf-8')
            _val34 = iprot.readString().decode('utf-8')
            self.attributes[_key33] = _val34
          iprot.readMapEnd()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Sentence')
    if self.tree is not None:
      oprot.writeFieldBegin('tree', TType.STRING, 1)
      oprot.writeString(self.tree.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.tokens is not None:
      oprot.writeFieldBegin('tokens', TType.LIST, 2)
      oprot.writeListBegin(TType.STRUCT, len(self.tokens))
      for iter35 in self.tokens:
        iter35.write(oprot)
      oprot.writeListEnd()
      oprot.writeFieldEnd()
    if self.dependencies is not None:
      oprot.writeFieldBegin('dependencies', TType.MAP, 3)
      oprot.writeMapBegin(TType.STRING, TType.LIST, len(self.dependencies))
      for kiter36,viter37 in self.dependencies.items():
        oprot.writeString(kiter36.encode('utf-8'))
        oprot.writeListBegin(TType.STRUCT, len(viter37))
        for iter38 in viter37:
          iter38.write(oprot)
        oprot.writeListEnd()
      oprot.writeMapEnd()
      oprot.writeFieldEnd()
    if self.index is not None:
      oprot.writeFieldBegin('index', TType.I32, 4)
      oprot.writeI32(self.index)
      oprot.writeFieldEnd()
    if self.sentiment is not None:
      oprot.writeFieldBegin('sentiment', TType.STRING, 5)
      oprot.writeString(self.sentiment.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.nexts is not None:
      oprot.writeFieldBegin('nexts', TType.I32, 6)
      oprot.writeI32(self.nexts)
      oprot.writeFieldEnd()
    if self.previous is not None:
      oprot.writeFieldBegin('previous', TType.I32, 7)
      oprot.writeI32(self.previous)
      oprot.writeFieldEnd()
    if self.attributes is not None:
      oprot.writeFieldBegin('attributes', TType.MAP, 8)
      oprot.writeMapBegin(TType.STRING, TType.STRING, len(self.attributes))
      for kiter39,viter40 in self.attributes.items():
        oprot.writeString(kiter39.encode('utf-8'))
        oprot.writeString(viter40.encode('utf-8'))
      oprot.writeMapEnd()
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.tree)
    value = (value * 31) ^ hash(self.tokens)
    value = (value * 31) ^ hash(self.dependencies)
    value = (value * 31) ^ hash(self.index)
    value = (value * 31) ^ hash(self.sentiment)
    value = (value * 31) ^ hash(self.nexts)
    value = (value * 31) ^ hash(self.previous)
    value = (value * 31) ^ hash(self.attributes)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class Span(object):
  """
  Attributes:
   - head
   - tail
   - sent_index
  """

  __slots__ = [ 
    'head',
    'tail',
    'sent_index',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.I32, 'head', None, None, ), # 1
    (2, TType.I32, 'tail', None, None, ), # 2
    (3, TType.I32, 'sent_index', None, None, ), # 3
  )

  def __init__(self, head=None, tail=None, sent_index=None,):
    self.head = head
    self.tail = tail
    self.sent_index = sent_index

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.I32:
          self.head = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I32:
          self.tail = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.I32:
          self.sent_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Span')
    if self.head is not None:
      oprot.writeFieldBegin('head', TType.I32, 1)
      oprot.writeI32(self.head)
      oprot.writeFieldEnd()
    if self.tail is not None:
      oprot.writeFieldBegin('tail', TType.I32, 2)
      oprot.writeI32(self.tail)
      oprot.writeFieldEnd()
    if self.sent_index is not None:
      oprot.writeFieldBegin('sent_index', TType.I32, 3)
      oprot.writeI32(self.sent_index)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.head)
    value = (value * 31) ^ hash(self.tail)
    value = (value * 31) ^ hash(self.sent_index)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class Suggestion(object):
  """
  Attributes:
   - start_index
   - end_index
   - message
   - corrections
   - id
   - category
  """

  __slots__ = [ 
    'start_index',
    'end_index',
    'message',
    'corrections',
    'id',
    'category',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.I32, 'start_index', None, None, ), # 1
    (2, TType.I32, 'end_index', None, None, ), # 2
    (3, TType.STRING, 'message', None, None, ), # 3
    (4, TType.LIST, 'corrections', (TType.STRING,None), None, ), # 4
    (5, TType.STRING, 'id', None, None, ), # 5
    (6, TType.STRING, 'category', None, None, ), # 6
  )

  def __init__(self, start_index=None, end_index=None, message=None, corrections=None, id=None, category=None,):
    self.start_index = start_index
    self.end_index = end_index
    self.message = message
    self.corrections = corrections
    self.id = id
    self.category = category

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.I32:
          self.start_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I32:
          self.end_index = iprot.readI32()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.STRING:
          self.message = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.LIST:
          self.corrections = []
          (_etype44, _size41) = iprot.readListBegin()
          for _i45 in xrange(_size41):
            _elem46 = iprot.readString().decode('utf-8')
            self.corrections.append(_elem46)
          iprot.readListEnd()
        else:
          iprot.skip(ftype)
      elif fid == 5:
        if ftype == TType.STRING:
          self.id = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      elif fid == 6:
        if ftype == TType.STRING:
          self.category = iprot.readString().decode('utf-8')
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('Suggestion')
    if self.start_index is not None:
      oprot.writeFieldBegin('start_index', TType.I32, 1)
      oprot.writeI32(self.start_index)
      oprot.writeFieldEnd()
    if self.end_index is not None:
      oprot.writeFieldBegin('end_index', TType.I32, 2)
      oprot.writeI32(self.end_index)
      oprot.writeFieldEnd()
    if self.message is not None:
      oprot.writeFieldBegin('message', TType.STRING, 3)
      oprot.writeString(self.message.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.corrections is not None:
      oprot.writeFieldBegin('corrections', TType.LIST, 4)
      oprot.writeListBegin(TType.STRING, len(self.corrections))
      for iter47 in self.corrections:
        oprot.writeString(iter47.encode('utf-8'))
      oprot.writeListEnd()
      oprot.writeFieldEnd()
    if self.id is not None:
      oprot.writeFieldBegin('id', TType.STRING, 5)
      oprot.writeString(self.id.encode('utf-8'))
      oprot.writeFieldEnd()
    if self.category is not None:
      oprot.writeFieldBegin('category', TType.STRING, 6)
      oprot.writeString(self.category.encode('utf-8'))
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.start_index)
    value = (value * 31) ^ hash(self.end_index)
    value = (value * 31) ^ hash(self.message)
    value = (value * 31) ^ hash(self.corrections)
    value = (value * 31) ^ hash(self.id)
    value = (value * 31) ^ hash(self.category)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class FullParse(object):
  """
  Attributes:
   - sentences
   - suggestions
   - corefs
   - attributes
  """

  __slots__ = [ 
    'sentences',
    'suggestions',
    'corefs',
    'attributes',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.LIST, 'sentences', (TType.STRUCT,(Sentence, Sentence.thrift_spec)), None, ), # 1
    (2, TType.LIST, 'suggestions', (TType.STRUCT,(Suggestion, Suggestion.thrift_spec)), None, ), # 2
    (3, TType.LIST, 'corefs', (TType.LIST,(TType.STRUCT,(Span, Span.thrift_spec))), None, ), # 3
    (4, TType.MAP, 'attributes', (TType.STRING,None,TType.STRING,None), {
    }, ), # 4
  )

  def __init__(self, sentences=None, suggestions=None, corefs=None, attributes=thrift_spec[4][4],):
    self.sentences = sentences
    self.suggestions = suggestions
    self.corefs = corefs
    if attributes is self.thrift_spec[4][4]:
      attributes = {
    }
    self.attributes = attributes

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.LIST:
          self.sentences = []
          (_etype51, _size48) = iprot.readListBegin()
          for _i52 in xrange(_size48):
            _elem53 = Sentence()
            _elem53.read(iprot)
            self.sentences.append(_elem53)
          iprot.readListEnd()
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.LIST:
          self.suggestions = []
          (_etype57, _size54) = iprot.readListBegin()
          for _i58 in xrange(_size54):
            _elem59 = Suggestion()
            _elem59.read(iprot)
            self.suggestions.append(_elem59)
          iprot.readListEnd()
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.LIST:
          self.corefs = []
          (_etype63, _size60) = iprot.readListBegin()
          for _i64 in xrange(_size60):
            _elem65 = []
            (_etype69, _size66) = iprot.readListBegin()
            for _i70 in xrange(_size66):
              _elem71 = Span()
              _elem71.read(iprot)
              _elem65.append(_elem71)
            iprot.readListEnd()
            self.corefs.append(_elem65)
          iprot.readListEnd()
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.MAP:
          self.attributes = {}
          (_ktype73, _vtype74, _size72 ) = iprot.readMapBegin()
          for _i76 in xrange(_size72):
            _key77 = iprot.readString().decode('utf-8')
            _val78 = iprot.readString().decode('utf-8')
            self.attributes[_key77] = _val78
          iprot.readMapEnd()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('FullParse')
    if self.sentences is not None:
      oprot.writeFieldBegin('sentences', TType.LIST, 1)
      oprot.writeListBegin(TType.STRUCT, len(self.sentences))
      for iter79 in self.sentences:
        iter79.write(oprot)
      oprot.writeListEnd()
      oprot.writeFieldEnd()
    if self.suggestions is not None:
      oprot.writeFieldBegin('suggestions', TType.LIST, 2)
      oprot.writeListBegin(TType.STRUCT, len(self.suggestions))
      for iter80 in self.suggestions:
        iter80.write(oprot)
      oprot.writeListEnd()
      oprot.writeFieldEnd()
    if self.corefs is not None:
      oprot.writeFieldBegin('corefs', TType.LIST, 3)
      oprot.writeListBegin(TType.LIST, len(self.corefs))
      for iter81 in self.corefs:
        oprot.writeListBegin(TType.STRUCT, len(iter81))
        for iter82 in iter81:
          iter82.write(oprot)
        oprot.writeListEnd()
      oprot.writeListEnd()
      oprot.writeFieldEnd()
    if self.attributes is not None:
      oprot.writeFieldBegin('attributes', TType.MAP, 4)
      oprot.writeMapBegin(TType.STRING, TType.STRING, len(self.attributes))
      for kiter83,viter84 in self.attributes.items():
        oprot.writeString(kiter83.encode('utf-8'))
        oprot.writeString(viter84.encode('utf-8'))
      oprot.writeMapEnd()
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.sentences)
    value = (value * 31) ^ hash(self.suggestions)
    value = (value * 31) ^ hash(self.corefs)
    value = (value * 31) ^ hash(self.attributes)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)


class SerializedException(TException):
  """
  Attributes:
   - payload
  """

  __slots__ = [ 
    'payload',
   ]

  thrift_spec = (
    None, # 0
    (1, TType.STRING, 'payload', None, None, ), # 1
  )

  def __init__(self, payload=None,):
    self.payload = payload

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.STRING:
          self.payload = iprot.readString()
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('SerializedException')
    if self.payload is not None:
      oprot.writeFieldBegin('payload', TType.STRING, 1)
      oprot.writeString(self.payload)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    if self.payload is None:
      raise TProtocol.TProtocolException(message='Required field payload is unset!')
    return


  def __str__(self):
    return repr(self)

  def __hash__(self):
    value = 17
    value = (value * 31) ^ hash(self.payload)
    return value

  def __repr__(self):
    L = ['%s=%r' % (key, getattr(self, key))
      for key in self.__slots__]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    if not isinstance(other, self.__class__):
      return False
    for attr in self.__slots__:
      my_val = getattr(self, attr)
      other_val = getattr(other, attr)
      if my_val != other_val:
        return False
    return True

  def __ne__(self, other):
    return not (self == other)

