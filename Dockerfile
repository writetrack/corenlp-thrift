FROM       zezuladp/thrift
MAINTAINER Dan Zezula <dan@writelab.com>

ADD . /opt/

WORKDIR /opt

ENTRYPOINT ["./scripts/start_server.sh"]
