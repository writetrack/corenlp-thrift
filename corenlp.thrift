namespace java CoreNLP
namespace py corenlp

struct Token {
    1:string word,
    2:string pos,
    3:string lemma,
    4:i32 start_index,
    5:i32 end_index,
    6:i32 index,
    7: optional i32 nextt,
    8: optional i32 previous,
    9: optional map<string, string> attributes = {}
}

struct Dependency {
    2:i32 governor_index,
    4:i32 dependent_index
}

struct Sentence {
    1:string tree,
    2:list<Token> tokens,
    3:map<string, list<Dependency>> dependencies,
    4:i32 index,
    5: optional string sentiment,
    6: optional i32 nexts,
    7: optional i32 previous,
    8: optional map<string, string> attributes = {}
}

struct Span {
    1:i32 head,
    2:i32 tail,
    3:i32 sent_index
}

struct Suggestion {
    1:i32 start_index,
    2:i32 end_index,
    3:string message,
    4:list<string> corrections,
    5:string id,
    6:string category
}

struct FullParse {
    1:list<Sentence> sentences,
    2:list<Suggestion> suggestions,
    3: optional list<list<Span>> corefs,
    4: optional map<string, string> attributes = {}
}

exception SerializedException {
    1: required binary payload
}

service StanfordCoreNLP {
    void ping(),
    oneway void zip(),
    FullParse parse_text(1:string text, 2:list<string> outputOptions),
}
