# WriteLab Testing (Multiprocess)
# Author: William Guo

from corenlp import StanfordCoreNLP
from thrift.transport import TSocket, TTransport
from thrift.protocol import TBinaryProtocol
import multiprocessing
from multiprocessing import Process, Pipe
import time
import sys, getopt

class MultTest(object):
    
    def __init__(self, name):
        self.name = name

    def parse_text(self, data, ip):
        # Store process name as a variable for future reference.
        proc_name = multiprocessing.current_process().name

        # Do processing & parse, print messages.
        print 'Parsing data for process %s in test %s.' % (proc_name, self.name)

        # Start timer to track time elapsed for parsing/processing.
        start = time.time()

        # Handle request.
        self.thrift_request(data, ip)

        # End timer for time elapsed.
        end = time.time()
        elapsed = end - start

        print "Time elapsed for process %s in test %s:" % (proc_name, self.name) + " " + str(elapsed)
        return True

    def thrift_request(self, data, ip):
        # transport = TSocket.TSocket('localhost', 8090) # Server IP: "IPAddress": "172.17.0.21",
        transport = TSocket.TSocket(ip, 8090)
        transport = TTransport.TBufferedTransport(transport)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)
        client = StanfordCoreNLP.Client(protocol)
        transport.open() 
        result = client.parse_text(data, ['-outputFormat', 'typedDependencies,penn,wordsAndTags', '-outputFormatOptions', 'includePunctuationDependencies'])
        transport.close()
        f = open("output.txt", "a")
        f.write(str(result))
        f.close()

def worker(conn, data, ip):
    conn.send(MultTest('Testing').parse_text(data, ip))
    conn.close()

def divider():
    return "****************************************************************************"

if __name__ == '__main__':

    # Ignore SIGPIPE signals (multiprocessing).
    from signal import signal, SIGPIPE, SIG_DFL
    signal(SIGPIPE,SIG_DFL) 
    
    # Opens up new output file.
    f = open("output.txt", "w")
    f.close()

    consecutive = 0
    runtime_param = 1
    num = 1
    processes = 1
    data = ""
    filename = "johnson.txt"
    ip = "localhost"

    # Get-opt Parsing.
    # opts to specify OPTIONS, args to specify ARGUMENTS
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'cf:p:t:n:h:')
    except getopt.GetoptError as err:
        print str(err) # will print something like "option -a not recognized"
        sys.exit(2)

    # Flags:
    # -c (optional): Specify that the processes will be run consecutively (serial processing).
    # -p <num> (optional): Specify how many processes to spawn. If not specified, this value defaults to 1.
    # -t <num> (optional): Specify how many times to run this test. If unspecified, this value defaults to 1.
    # -f <filename> (mandatory): Specify file to use.
    # -n <num> (optional): Number of Johnsons (files) to concatenate.
    # -h <ip> (optional): Specify ip address corresponding to container.

    for opt, val in opts:
        if opt == "-c":
            consecutive = 1
        elif opt == "-p":
            processes = val
        elif opt == "-t":
            runtime_param = val
        elif opt == "-f":
            filename = val
        elif opt == "-n":
            num = val
        elif opt == "-h":
            ip = val
        else:
            assert False, "Unhandled option."

    con_str = "Parallel"
    sent_str = "Unspecified Length"
    serv_str = "Apache Thrift"

    if consecutive == 1:
        con_str = "Serial"

    if filename == "":
        print ("Invalid file.")
        sys.exit(2)

    print "\n" + divider()

    print "Running Test " + str(runtime_param) + " times on " + serv_str + " server."
    print "Starting " + str(processes) + " Processes in " + con_str + ", using " + str(num) + " " + "Johnsons."
    print divider()

    # Build data based on Johnson.txt.
    if filename != "":
        contents = ""
        with open(str(filename), "rb") as read_file:
            contents = read_file.read()

        data = '{"text":'

        # Concatenate depending on number of specified Johnson files.
        for concat in xrange(int(num)):
            data += contents

        data += '}'

    else:
        print "Error. Please try again."
        sys.exit(2)

    # Spawn workers and run.
    for run in xrange(int(runtime_param)):
        print "ITERATION " + str(run + 1) + ":"
        jobs = []

        if consecutive == 1:
            for process in xrange(int(processes)):
                parent_conn, child_conn = Pipe()
                proc = Process(target=worker, args=(child_conn, data, ip))
                proc.start()
                proc.join()
            print "Completed."

        else:
            for process in xrange(int(processes)):
                parent_conn, child_conn = Pipe()
                proc = Process(target=worker, args=(child_conn, data, ip))
                jobs.append(proc)

            # Start all processes at same time (sort of).
            for job in jobs:
                job.start()
            # Check to make sure that all processes finish.
            for job in jobs:
                job.join()
            print "Completed."
        
        print divider()
