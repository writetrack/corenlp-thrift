#!/bin/bash

CLASSPATH=
for i in `ls jars/`
do
  CLASSPATH=${CLASSPATH}:$PWD/'jars'/${i}
done

MAINDIR=$(dirname $0:A)/../
if [ $# -eq 3 ]; then
 PORT=$1
 HEAPSIZE=$2
 CONFIG=$3
else
 echo "No variables passed, using env variables HEAPSIZE PORT and CONFIG"
fi
java -cp $CLASSPATH:$MAINDIR/stanford-corenlp-wrapper.jar -Xmx$HEAPSIZE -XX:-UseGCOverheadLimit StanfordCoreNLPServer $PORT $CONFIG


