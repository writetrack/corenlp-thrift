#!/usr/bin/env python
# -*- coding: utf-8 -*-

# The purpose of this client is to show how to send over a few sentences in every way possible.
# It is also for me to unit test things. >:)
import sys
from corenlp import StanfordCoreNLP
from corenlp.ttypes import *
from thrift import Thrift
from thrift.transport import TSocket, TTransport
from thrift.protocol import TBinaryProtocol

# get command line arguments
args = sys.argv[1:]
if len(args) != 2:
    sys.stderr.write('Usage: parser_client.py <server> <port>\n')
    sys.exit(2)
else:
    server = args[0]
    port = int(args[1])


# Make socket
transport = TSocket.TSocket(server, port)

# Buffering is critical. Raw sockets are very slow
transport = TTransport.TBufferedTransport(transport)

# Wrap in a protocol
protocol = TBinaryProtocol.TBinaryProtocol(transport)

# Create a client to use the protocol encoder
client = StanfordCoreNLP.Client(protocol)

# Connect!
transport.open()

# This list is for options for how we'd like the output formatted.  See README.md for the full list of possible options.
# Note that the DEFAULT is what you would get if you specified "oneline" on the command line, or "None" here.
# You have to pass in something, and unfortunately it doesn't seem like that something can be None or an empty list.
# See http://diwakergupta.github.io/thrift-missing-guide/#_defining_structs for a possible explanation as to why...
# So, the following examples are VALID values for the second argument to these parse_* methods.
# (There are, of course, many more valid combinations depending on what the Stanford Parser supports.)
outputOptions = [
    'coref',
    'sentiment']
#outputOptions = ["-outputFormat", "oneline"]
#outputOptions = ["-outputFormat", "typedDependencies"]
outputOptions = ["coref", "sentiment"]
outputOptions = ["coref", "languagetool"]
with open('johnson.txt', 'rb') as f:
    text = f.read()

#text = 'Will said "I tried using quotes".'
#text = 'Just as Socrates felt that it was necessary to create a tension in the mind so that individuals could rise from the bondage of myths and half truths to the unfettered realm of creative analysis and objective appraisal, so must we see the need for nonviolent gadflies to create the kind of tension in society that will help men rise from the dark depths of prejudice and racism to the majestic heights of understanding and brotherhood.'
text = 'They is playing cricket.'

parse = client.parse_text(text, outputOptions)
print parse

# All done
transport.close()

